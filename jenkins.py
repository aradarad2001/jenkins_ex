import sys
import time


def seq(num):
    for i in range(int(num)):
        print(i, i**2)
        time.sleep(1)


def main():
    if len(sys.argv) < 2:
        exit("No file path was entered")
    elif len(sys.argv) > 2:
        exit("More than one file path was entered")
    else:
        seq(sys.argv[1])


if __name__ == '__main__':
    main()
